/**
 * @author Xavi Bonet
 */

package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio5App {

	public static void main(String[] args) {

		String text = JOptionPane.showInputDialog("Introduce un numero: ");
		double num = Double.parseDouble(text);
		
		if ( num%2 == 0 ) {
			JOptionPane.showMessageDialog(null, "El numero " + text + " es divisible entre 2");
		} else {
			JOptionPane.showMessageDialog(null, "El numero " + text + " no es divisible entre 2");
		}
		
	}

}
