/**
 * @author Xavi Bonet
 */

package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio12App {

	public static void main(String[] args) {
		
		final String PASSWORD = "password";
		
		for (int i = 1 ; i <= 3 ; i++) {
			String text = JOptionPane.showInputDialog("Introduce la contraseņa: ");
			if (text.equals(PASSWORD)) {
				JOptionPane.showMessageDialog(null, "Enhorabuena");
				i = 10;
			}
		}

	}

}
