/**
 * @author Xavi Bonet
 */

package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio3App {

	public static void main(String[] args) {
		
		String name = JOptionPane.showInputDialog("Introduce tu nombre:");
		
		JOptionPane.showMessageDialog(null, "Bienvenido " + name);
		System.out.println("Bienvenido " + name);
		
	}

}