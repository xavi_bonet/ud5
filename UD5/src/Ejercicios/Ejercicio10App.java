/**
 * @author Xavi Bonet
 */

package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio10App {

	public static void main(String[] args) {
		
		String textNumVentas = JOptionPane.showInputDialog("Introduce el n�mero de ventas: ");
		int numVentas = Integer.parseInt(textNumVentas);	
		double totalVentas = 0;
		for (int i = 1 ; i <= numVentas ; i++) {
			String textPrecioVenta = JOptionPane.showInputDialog("Introduce el precio de la venta: ");
			double precioVenta = Double.parseDouble(textPrecioVenta);	
			totalVentas = totalVentas + precioVenta;
		}
		JOptionPane.showMessageDialog(null, "El total de las ventas es: " + totalVentas);
		
	}

}
