/**
 * @author Xavi Bonet
 */

package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio4App {

	public static void main(String[] args) {
		
		String text = JOptionPane.showInputDialog("Introduce el radio: ");
		JOptionPane.showMessageDialog(null, "El �rea del circulo es: " + Math.PI * Math.pow(Double.parseDouble(text) , 2));
		
	}

}
