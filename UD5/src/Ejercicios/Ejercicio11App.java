/**
 * @author Xavi Bonet
 */

package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio11App {

	public static void main(String[] args) {
		
		String diaSemana = JOptionPane.showInputDialog("Introduce el dia de la semana: ");
		
		switch (diaSemana) {
			case "Lunes":
				JOptionPane.showMessageDialog(null, "El lunes es un dia laborable");
				break;
			case "Martes":
				JOptionPane.showMessageDialog(null, "El martes es un dia laborable");
				break;
			case "Miercoles":
				JOptionPane.showMessageDialog(null, "El miercoles es un dia laborable");
				break;
			case "Jueves":
				JOptionPane.showMessageDialog(null, "El jueves es un dia laborable");
				break;
			case "Viernes":
				JOptionPane.showMessageDialog(null, "El viernes es un dia laborable");
				break;
			case "Sabado":
				JOptionPane.showMessageDialog(null, "El sabado no es un dia laborable");
				break;
			case "Domingo":
				JOptionPane.showMessageDialog(null, "El domingo no es un dia laborable");
				break;
			default:
				JOptionPane.showMessageDialog(null, "El dia introducido no es valido");
		}
		
	}

}
