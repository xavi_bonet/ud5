/**
 * @author Xavi Bonet
 */

package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio6App {

	public static void main(String[] args) {

		final double IVA = 0.21;
		
		String text = JOptionPane.showInputDialog("Introduce el precio: ");
		double precioProducto = Double.parseDouble(text);
		double ivaProducto = precioProducto * IVA;
		double precioFinal = precioProducto + ivaProducto;
		
		JOptionPane.showMessageDialog(null, "El precio con IVA es: " + precioFinal);
	
	}

}
