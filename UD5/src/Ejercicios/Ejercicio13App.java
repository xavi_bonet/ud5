/**
 * @author Xavi Bonet
 */

package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio13App {

	public static void main(String[] args) {
		
		int num1 = Integer.parseInt(JOptionPane.showInputDialog("Introduce el primer numero: "));
		int num2 = Integer.parseInt(JOptionPane.showInputDialog("Introduce el primer numero: "));
		String signo = JOptionPane.showInputDialog("Introduce el tipo de operacion: ( + , - , * , / , ^ , %)");

		switch (signo) {
			case "+":
				JOptionPane.showMessageDialog(null, "La suma de " + num1 + " y " + num2 + " es " + (num1 + num2));
				break;
			case "-":
				JOptionPane.showMessageDialog(null, "La resta de " + num1 + " y " + num2 + " es " + (num1 - num2));
				break;
			case "*":
				JOptionPane.showMessageDialog(null, "La multiplicacion de " + num1 + " y " + num2 + " es " + (num1 * num2));
				break;
			case "/":
				JOptionPane.showMessageDialog(null, "La divicion de " + num1 + " y " + num2 + " es " + ((double)num1 / num2));
				break;
			case "^":
				JOptionPane.showMessageDialog(null, num1 + " elevado a " + num2 + " es " + (Math.pow(num1, num2)));
				break; 
			case "%":
				JOptionPane.showMessageDialog(null, "El resto de la division entre " + num1 + " y " + num2 + " es " + (num1 % num2));
				break;
			default:
				JOptionPane.showMessageDialog(null, "El simbolo introducido no es valido");
		}
		
	}

}
