/**
 * @author Xavi Bonet
 */

package Ejercicios;

import javax.swing.JOptionPane;

public class Ejercicio1App {

	public static void main(String[] args) {
		
		int num1 = 15;
		int num2 = 10;
		
		if (num1 > num2) {
			JOptionPane.showMessageDialog(null, " El numero 1 ( " + num1 + " ) es mas grande que el numero 2 ( " + num2 + " )");
			System.out.println(" El numero 1 ( " + num1 + " ) es mas grande que el numero 2 ( " + num2 + " )");
		} else if (num2 > num1) {
			JOptionPane.showMessageDialog(null, " El numero 2 ( " + num2 + " ) es mas grande que el numero 1 ( " + num1 + " )");
			System.out.println(" El numero 2 ( " + num2 + " ) es mas grande que el numero 1 ( " + num1 + " )");
		} else if (num1 == num2) {
			JOptionPane.showMessageDialog(null, " El numero 1 ( " + num1 + " ) es igual que el numero 2 ( " + num2 + " )");
			System.out.println(" El numero 1 ( " + num1 + " ) es igual que el numero 2 ( " + num2 + " )");
		}

	}

}
