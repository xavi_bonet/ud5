/**
 * @author Xavi Bonet
 */

package Ejercicios;

public class Ejercicio7App {

	public static void main(String[] args) {
		
		int num = 1;
		
		while (num<=100) {
			System.out.println(num);
			num++;
		}
		
	}

}
